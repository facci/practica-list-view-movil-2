import 'package:flutter/material.dart';

import '../model/character.dart';


class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15.0),
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 214, 208, 208),
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset(
              character.image,
              height: 110.0,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 10.0),
                child: Text(
                  character.name,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 29.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                    color: Color.fromARGB(255, 7, 18, 111),
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: character.stars < 4.0
                          ? const Color.fromARGB(255, 203, 22, 37)
                          : const Color.fromARGB(255, 11, 208, 70),
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      character.stars.toString(),
                      style:
                      const TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      character.jobTitle,
                      style: const TextStyle(
                        fontSize: 20.0,
                        color: Color.fromARGB(255, 198, 35, 168),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
